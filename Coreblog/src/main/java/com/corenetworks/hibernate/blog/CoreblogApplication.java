package com.corenetworks.hibernate.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreblogApplication.class, args);
	}
}