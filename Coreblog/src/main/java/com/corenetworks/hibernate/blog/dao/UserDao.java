package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.config.model.User;



@Repository
@Transactional
public class UserDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el usuario en la base de datos
     */
    public void create(User usuario) {
    	entityManager.persist(usuario);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<User> getAll(){
    	return entityManager
    			.createQuery("select u from User u")
    			.getResultList();
    }
    
    
    /*
     * Validar login a partir de email y password
     */
    public User getByEmailAndPassword(String email, String password) {
    	User resultado = null;
    	try {
    	resultado = (User)  entityManager
    	    .createNativeQuery("select * FROM user where  email= :email and password=md5(:password)",
    	    		User.class)
    	     .setParameter("email", email)
    	     .setParameter("password", password)
    	     .getSingleResult();
    	} catch (NoResultException e) {
    		resultado = null;
		}
    	return resultado;
    	
    }
    
    
    
}